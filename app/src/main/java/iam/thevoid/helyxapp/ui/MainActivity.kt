package iam.thevoid.helyxapp.ui

import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.widget.ImageView
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import iam.thevoid.ae.actionBarHeight
import iam.thevoid.ae.drawable
import iam.thevoid.helyxapp.R
import iam.thevoid.helyxapp.api.model.Controller
import iam.thevoid.helyxapp.ui.vm.ControllerViewModel
import io.reactivex.Flowable
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import ru.butik.android.ui.mvvm.vm.viewModel
import thevoid.iam.rx.widget.ext.*

class MainActivity : AppCompatActivity() {

    private val vm by viewModel<ControllerViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout {

            noConnectLayout {
                noConnectIcon()
                noConnectTitle()
            }

            stateLayout {

                helyxToolbar()

                contentLayout {
                    manualLightButton()

                    lightIndicatorsLayout {
                        lightIndicatorLayout {
                            lightIndicatorTitle(R.string.auto)
                            lightIndicator { it.state.lightAuto }
                        }
                        lightIndicatorLayout {
                            lightIndicatorTitle(R.string.manual)
                            lightIndicator { it.state.lightManual }
                        }
                    }

                    divider()

                    tumblerStateTitle()

                    tumblerStateLayout {
                        tumblerStateText(R.string.left) { it.state.switchLeft }
                        tumblerStateText(R.string.up) { it.state.switchOff() && !it.isEmpty() }
                        tumblerStateText(R.string.right) { it.state.switchRight }
                    }

                    counterDivider()

                    counterLayout {
                        counterTitle()
                        counterValue()
                    }
                }
            }
            description()
            logo()
        })
    }

    private fun layout(init: _FrameLayout.() -> Unit) =
        frameLayout {
            backgroundColorResource = android.R.color.white
            init()
        }

    private fun _FrameLayout.logo() =
        imageView {
            hideWhenLoaded(vm.splashLoading)
            padding = dip(56)
            backgroundColorResource = R.color.colorPrimary
            imageResource = R.drawable.logo
        }.lparams(matchParent, matchParent)

    private fun _FrameLayout.description() =
        textView {
            gravity = Gravity.CENTER_HORIZONTAL
            alpha = .5f
            setTypeface(null, Typeface.BOLD)
            textResource = R.string.c_system
        }.lparams(matchParent, wrapContent, Gravity.BOTTOM) {
            margin = dip(16)
        }

    private fun _FrameLayout.stateLayout(init: _LinearLayout.() -> Unit) =
        verticalLayout {
            gone(vm.controller.map { it.isEmpty() })
            init()
        }

    private fun _LinearLayout.contentLayout(init: _LinearLayout.() -> Unit) =
        verticalLayout {
            gone(vm.controller.map { it.isEmpty() })
            padding = dip(8)
            init()
        }


    private fun _FrameLayout.noConnectLayout(init: _LinearLayout.() -> Unit) =
        verticalLayout {
            gone(vm.controller.map { !it.isEmpty() })
            alpha = .3f
            init()
        }.lparams(wrapContent, wrapContent, Gravity.CENTER)

    private fun _LinearLayout.noConnectIcon() =
        imageView {
            imageResource = R.drawable.ic_highlight_off_black_24dp
        }.lparams(dip(64), dip(64)) {
            gravity = Gravity.CENTER_HORIZONTAL
        }

    private fun _LinearLayout.helyxToolbar() =
        toolbar {
            backgroundColorResource = R.color.colorPrimary
            titleResource = R.string.app_name
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                elevation = dip(8).toFloat()
                setTitleTextColor(Color.WHITE)
            }
        }.lparams(matchParent, actionBarHeight)

    private fun _LinearLayout.noConnectTitle() =
        textView {
            setTypeface(null, Typeface.BOLD)
            textSize = 20f
            allCaps = true
            textColor = Color.BLACK
            textResource = R.string.no_connection
        }.lparams(wrapContent, wrapContent) {
            gravity = Gravity.CENTER_HORIZONTAL
            topMargin = dip(32)
        }

    private fun _LinearLayout.counterLayout(init: _FrameLayout.() -> Unit) =
        frameLayout(init).lparams(matchParent, wrapContent)


    private fun _FrameLayout.counterTitle() =
        textView {
            padding = dip(8)
            textResource = R.string.actions_count
            allCaps = true
            textSize = 16f
            textColor = Color.BLACK
        }.lparams(wrapContent, wrapContent, Gravity.BOTTOM)

    private fun _FrameLayout.counterValue() =
        textView {
            textSize = 32f
            padding = dip(8)
            setText(vm.controller.map { "${it.actionCounter}" })
            allCaps = true
            setTypeface(null, Typeface.BOLD)
            textColor = Color.BLACK
        }.lparams(wrapContent, wrapContent, Gravity.END)

    private fun _LinearLayout.counterDivider() =
        divider().apply { gone(vm.controller.map { it.isEmpty() }) }


    private fun _LinearLayout.manualLightButton() =
        button {
            //            setEnabled(vm.controller.map { !it.isEmpty() })
            background = drawable(R.drawable.button_background)
            textResource = R.string.manual_light
            textColor = Color.WHITE
            setOnClickListener { vm.toggleState() }
        }.lparams(matchParent, wrapContent) {
            topMargin = dip(24)
        }


    private fun _LinearLayout.divider() =
        view {
            backgroundColor = Color.LTGRAY
        }.lparams(matchParent, dip(1)) {
            topMargin = dip(16)
            bottomMargin = dip(16)
        }

    private fun _LinearLayout.lightIndicatorsLayout(init: _LinearLayout.() -> Unit) =
        linearLayout {
            weightSum = 1f
            init()
        }.lparams(matchParent, wrapContent) {
            topMargin = dip(16)
        }

    private fun _LinearLayout.lightIndicatorLayout(init: _LinearLayout.() -> Unit) =
        verticalLayout { init() }.lparams(0, wrapContent) {
            weight = .5f
        }

    private fun _LinearLayout.lightIndicatorTitle(@StringRes title: Int) =
        textView {
            gravity = Gravity.CENTER_HORIZONTAL
            padding = dip(8)
            textResource = title
            allCaps = true
            setTypeface(null, Typeface.BOLD)
            textColor = Color.BLACK
        }.lparams(matchParent, wrapContent)

    private fun _LinearLayout.lightIndicator(enableIf: (Controller) -> Boolean) =
        imageView {
            setImageResource(vm.controller.map { if (enableIf(it)) R.drawable.ic_brightness_high_black_24dp else R.drawable.ic_brightness_low_black_24dp })
        }.lparams(matchParent, dip(32)) {
            topMargin = dip(8)
        }

    private fun _LinearLayout.tumblerStateTitle() =
        textView {
            textSize = 16f
            textResource = R.string.tumbler_state
            allCaps = true
            textColor = Color.BLACK
        }.lparams(matchParent, wrapContent)

    private fun _LinearLayout.tumblerStateLayout(init: _LinearLayout.() -> Unit) =
        cardView {
            radius = dip(8).toFloat()
            cardElevation = dip(8).toFloat()
            linearLayout {
                weightSum = 3f
                init()
            }
        }.lparams(matchParent, wrapContent) {
            topMargin = dip(16)
            leftMargin = dip(4)
            rightMargin = dip(4)
        }

    private fun _LinearLayout.tumblerStateText(
        @StringRes textValue: Int,
        showIf: (Controller) -> Boolean
    ) =
        textView {
            textSize = 16f
            allCaps = true
            setTypeface(null, Typeface.BOLD)
            gravity = Gravity.CENTER_HORIZONTAL
            padding = dip(8)
            textResource = textValue
            setTextColor(vm.controller.map { if (showIf(it)) Color.BLACK else Color.LTGRAY })
        }.lparams(matchParent, wrapContent) {
            weight = 1f
        }

    private fun ImageView.setImageResource(flowable: Flowable<Int>) =
        addSetter(flowable) { imageResource = it }
}
