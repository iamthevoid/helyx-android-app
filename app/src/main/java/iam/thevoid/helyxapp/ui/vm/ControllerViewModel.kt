package iam.thevoid.helyxapp.ui.vm

import androidx.lifecycle.ViewModel
import com.jakewharton.rx.ReplayingShare
import iam.thevoid.helyxapp.api.ModbusApi
import iam.thevoid.helyxapp.api.model.Controller
import iam.thevoid.helyxapp.util.RxRetryWithDelay
import io.reactivex.Flowable
import io.reactivex.Single
import thevoid.iam.rx.rxdata.RxLoading
import thevoid.iam.rx.utils.loading
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.TimeUnit

class ControllerViewModel : ViewModel() {

    companion object {
        const val TIMEOUT_LOGO = 2000L
        const val TIMEOUT_RETRY = 500
        const val TIMEOUT_REPEAT = 100L
    }

    val splashLoading = RxLoading()

    private val splashDisposable =
        Single.timer(TIMEOUT_LOGO, TimeUnit.MILLISECONDS)
            .loading(splashLoading)
            .subscribe()

    private val queue = ConcurrentLinkedQueue<Single<Controller>>()
        .apply { add(ModbusApi.readState().onErrorReturnItem(Controller.EMPTY)) }

    fun toggleState() {
        queue.add(
            ModbusApi.writeState(controller.blockingFirst()
                .apply { toggleManualLight() })
                .onErrorReturnItem(Controller.EMPTY)
        )
        queue.add(ModbusApi.readState())
    }

    val controller: Flowable<Controller> = Single.defer<Controller> {
        if (queue.size == 1)
            queue.peek()
        else
            queue.poll()
    }.repeatWhen { it.delay(TIMEOUT_REPEAT, TimeUnit.MILLISECONDS) }
        .startWith(Controller.EMPTY)
        .retryWhen(RxRetryWithDelay(Int.MAX_VALUE, TIMEOUT_RETRY, TimeUnit.MILLISECONDS))
        .compose(ReplayingShare.instance<Controller>())

    override fun onCleared() {
        super.onCleared()
        splashDisposable.dispose()
    }
}