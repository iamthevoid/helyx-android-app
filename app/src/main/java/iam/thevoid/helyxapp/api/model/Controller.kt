package iam.thevoid.helyxapp.api.model

import com.intelligt.modbus.jlibmodbus.data.ModbusHoldingRegisters
import iam.thevoid.e.safe

data class Controller(
    val actionCounter: Int = -1,
    val state: State = State()
) {

    fun toggleManualLight() {
        if (isEmpty())
            return

        state.apply { lightManual = !lightManual }
    }

    data class State(
        val displayControlButton: Boolean = false,
        val switchRight: Boolean = false,
        val switchLeft: Boolean = false,
        val lightAuto: Boolean = false,
        var lightManual: Boolean = false,
        val dumbBit2: Boolean = false,
        val dumbBit3: Boolean = false,
        val dumbBit7: Boolean = false
    ) { fun switchOff() = !switchRight && !switchLeft }

    fun isEmpty() = EMPTY === this

    fun asRegisters(): IntArray =
        IntArray(3) { registerIndex ->
            when (registerIndex) {
                0 -> state.run {
                    dumbBit2.bit(DUMB_7) +
                            lightAuto.bit(LIGHT_AUTO) +
                            switchRight.bit(SWITCH_RIGHT) +
                            switchLeft.bit(SWITCH_LEFT) +
                            dumbBit3.bit(DUMB_3) +
                            dumbBit7.bit(DUMB_2) +
                            lightManual.bit(LIGHT_MANUAL) +
                            displayControlButton.bit(CONTROL_BUTTON)
                }
                2 -> actionCounter
                else -> 0
            }
        }

    companion object {

        // 0arl00mc

        const val CONTROL_BUTTON    = 0x0001
        const val LIGHT_MANUAL      = 0x0002
        const val DUMB_2            = 0x0004
        const val DUMB_3            = 0x0008
        const val SWITCH_LEFT       = 0x0010
        const val SWITCH_RIGHT      = 0x0020
        const val LIGHT_AUTO        = 0x0040
        const val DUMB_7            = 0x0080

        fun fromRegisters(mhr: ModbusHoldingRegisters): Controller = mhr.registers
            .run {
                Controller(
                    getOrNull(2).safe(),
                    firstOrNull().safe().run {
                        State(
                            bool(CONTROL_BUTTON),
                            bool(SWITCH_RIGHT),
                            bool(SWITCH_LEFT),
                            bool(LIGHT_AUTO),
                            bool(LIGHT_MANUAL),
                            bool(DUMB_2),
                            bool(DUMB_3),
                            bool(DUMB_7)
                        )
                    }
                )
            }

        private fun Int.bool(mask: Int) = (this and mask) > 0

        private fun Boolean.bit(mask: Int) = (if (this) 1 else 0) * mask

        val EMPTY = Controller()
    }
}