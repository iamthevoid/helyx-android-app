package iam.thevoid.helyxapp.api

import android.util.Log
import com.intelligt.modbus.jlibmodbus.Modbus
import com.intelligt.modbus.jlibmodbus.master.ModbusMasterFactory
import com.intelligt.modbus.jlibmodbus.msg.request.ReadHoldingRegistersRequest
import com.intelligt.modbus.jlibmodbus.msg.response.ReadHoldingRegistersResponse
import com.intelligt.modbus.jlibmodbus.tcp.TcpParameters
import iam.thevoid.helyxapp.api.model.Controller
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.net.InetAddress

object ModbusApi {

    private val initModbus: Unit by lazy {
        Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG)
        Modbus.setAutoIncrementTransactionId(true)
    }

    private val tcpParameters by lazy {
        TcpParameters().apply {
            host = InetAddress.getByName("192.168.0.201")
            port = Modbus.TCP_PORT
            isKeepAlive = true
        }
    }


    fun readState(): Single<Controller> =

        Single.create<Controller> {
            initModbus

            val master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters).apply {
                setResponseTimeout(1000)
            }

            val request = ReadHoldingRegistersRequest().apply {
                serverAddress = Modbus.TCP_DEFAULT_ID
                startAddress = 0
                quantity = 3
            }

            val response =
                request.response as ReadHoldingRegistersResponse

            try {
                master.connect()
                master.processRequest(request)
            } catch (e: Exception) {
                if (!it.isDisposed)
                    it.onError(e)
            } finally {
                master.disconnect()
            }

            Log.i("ModbusApi", "raw: ${response.holdingRegisters.joinToString { it.toString(2) }}")

            it.onSuccess(Controller.fromRegisters(response.holdingRegisters)
                .apply { Log.i("ModbusApi", asRegisters().joinToString { it.toString(2) }) })
        }.subscribeOn(Schedulers.io())


    fun writeState(state: Controller) =

        Single.create<Controller> {
            initModbus

            val master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters).apply {
                setResponseTimeout(1000)
            }

            try {
                master.connect()
                println(state.asRegisters().joinToString { it.toString(2) })
                master.writeMultipleRegisters(1, 0, state.asRegisters())
            } catch (e: Exception) {
                if (!it.isDisposed)
                    it.onError(e)
            } finally {
                master.disconnect()
            }
            it.onSuccess(state)
        }.subscribeOn(Schedulers.io())

}