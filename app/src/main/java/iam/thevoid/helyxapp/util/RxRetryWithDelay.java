package iam.thevoid.helyxapp.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

public class RxRetryWithDelay implements Function<Flowable<? extends Throwable>, Flowable<?>> {
    private final int maxRetries;
    private final int retryDelayMillis;
    private final TimeUnit timeUnit;
    private final boolean incremental;
    private AtomicInteger retryCount;
    private Predicate<Throwable> filter;

    public RxRetryWithDelay(final int maxRetries, final int delay, TimeUnit timeUnit) {
        this(maxRetries, delay, timeUnit, false);
    }

    public RxRetryWithDelay(final int maxRetries, final int delay, TimeUnit timeUnit, boolean incremental) {
        this.maxRetries = maxRetries;
        this.retryDelayMillis = delay;
        this.timeUnit = timeUnit;
        this.retryCount = new AtomicInteger();
        this.incremental = incremental;
    }

    public RxRetryWithDelay filter(Predicate<Throwable> predicate) {
        filter = predicate;
        return this;
    }

    @Override
    public Flowable<?> apply(final Flowable<? extends Throwable> attempts) {
        return attempts
                .flatMap(throwable -> {
                    if (filter == null || filter.test(throwable)) {
                        int count = retryCount.incrementAndGet();
                        if (count < maxRetries) {
                            // When this Observable calls onNext, the original
                            // Observable will be retried (i.e. re-subscribed).
                            return Flowable.timer(incremental ? retryDelayMillis * 2 ^ (count - 1) : retryDelayMillis, timeUnit);
                        }
                    }

                    // Max retries hit. Just pass the error along.
                    return Flowable.error(throwable);
                });
    }
}
