package iam.thevoid.helyxapp

import com.intelligt.modbus.jlibmodbus.Modbus
import com.intelligt.modbus.jlibmodbus.data.ModbusHoldingRegisters
import com.intelligt.modbus.jlibmodbus.exception.ModbusIOException
import com.intelligt.modbus.jlibmodbus.exception.ModbusNumberException
import com.intelligt.modbus.jlibmodbus.exception.ModbusProtocolException
import com.intelligt.modbus.jlibmodbus.master.ModbusMasterFactory
import com.intelligt.modbus.jlibmodbus.msg.request.ReadHoldingRegistersRequest
import com.intelligt.modbus.jlibmodbus.msg.response.ReadHoldingRegistersResponse
import com.intelligt.modbus.jlibmodbus.tcp.TcpParameters
import iam.thevoid.helyxapp.api.model.Controller
import org.junit.Test
import java.net.InetAddress
import java.net.UnknownHostException


class Modbus {

    private val tcpParameters by lazy {
        TcpParameters().apply {
            host = InetAddress.getByName("192.168.0.201")
            port = Modbus.TCP_PORT
            isKeepAlive = true
        }
    }

    @Test
    fun testModbusRefactored() {
        Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG)
        Modbus.setAutoIncrementTransactionId(true)

        val master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters).apply {
            setResponseTimeout(1000)
        }

        val request = ReadHoldingRegistersRequest().apply {
            serverAddress = Modbus.TCP_DEFAULT_ID
            startAddress = 0
            quantity = 3
        }

        val response =
            request.response as ReadHoldingRegistersResponse

        try {
            master.connect()
            master.processRequest(request)
            println(response.holdingRegisters.map { it.toString(2) })
            val message = Controller.fromRegisters(response.holdingRegisters)
            println(message)
            println(message.asRegisters().map { it.toString(2) })
        } catch (e: Exception) {
            throw e
        } finally {
            master.disconnect()
        }
    }

    @Test
    fun testModbus() {
        try {
            Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG)
            val tcpParameters = TcpParameters()
            //listening on localhost
            tcpParameters.host = InetAddress.getByName("192.168.0.201")
            tcpParameters.port = Modbus.TCP_PORT
            tcpParameters.isKeepAlive = true
            val master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters)
            master.setResponseTimeout(1000)
            val holdingRegisters = ModbusHoldingRegisters(3)
            for (i in 0 until holdingRegisters.quantity) { //fill
                holdingRegisters[i] = i + 1
            }
            Modbus.setAutoIncrementTransactionId(true)
            master.connect()
            //prepare request
            val request = ReadHoldingRegistersRequest()
            request.serverAddress = Modbus.TCP_DEFAULT_ID
            request.startAddress = 0
            request.quantity = 3
            val response =
                request.response as ReadHoldingRegistersResponse
            master.processRequest(request)
            val registers = response.holdingRegisters
            for (r in registers) {
                println(r.toString(2))
            }
            master.writeMultipleRegisters(1,0, intArrayOf(0b11010110))
            master.disconnect()

        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: ModbusProtocolException) {
            e.printStackTrace()
        } catch (e: ModbusIOException) {
            e.printStackTrace()
        } catch (e: ModbusNumberException) {
            e.printStackTrace()
        }
    }
}